import { initializeApp } from "https://www.gstatic.com/firebasejs/9.15.0/firebase-app.js";
import { getDatabase, ref, push, onValue, remove } from "https://www.gstatic.com/firebasejs/9.15.0/firebase-database.js"

const appSettings = {
    databaseURL: "https://playground-35b3b-default-rtdb.firebaseio.com"
}

const app = initializeApp(appSettings)
const database = getDatabase(app)
const shoppingListInDB = ref(database, "shoppingList")
// const booksInDB = ref(database, "books")


const inputFieldEl = document.getElementById("input-field");
const addButtonEl = document.getElementById("add-button");
const shoppingListEL = document.getElementById("shopping-list")

onValue(shoppingListInDB, function(snapshot) {


    if (snapshot.exists()) {
        let itemsArray = Object.entries(snapshot.val())
        clearshoppingListEl()

        for (let i=0; i<itemsArray.length; i++) {
            let currentItem = itemsArray[i]
            // let currentItemID = currentItem[0]
            // let currentItemValue = currentItem[1]
            appendItemToShoppingListEl(currentItem)
        }
    }
    else {
        shoppingListEL.innerHTML = "No items here... yet"
    }





})

addButtonEl.addEventListener("click", function() {
    let inputValue = inputFieldEl.value;

    push(shoppingListInDB, inputValue);
    clearInputFieldEl();
    // appendItemToShoppingListEl(inputValue);
    // shoppingListEL.innerHTML += `<li>${inputValue}</li>`


})


function clearInputFieldEl() {
    inputFieldEl.value = ""
}

function appendItemToShoppingListEl(item) {
    // shoppingListEL.innerHTML += `<li>${itemValue}</li>`
    let itemID = item[0]
    let itemValue = item[1]
    let newEl = document.createElement("li")
    newEl.textContent = itemValue

    newEl.addEventListener("click", function() {
        let exactLocationOfItemInDB = ref(database, `shoppingList/${itemID}`)
        remove(exactLocationOfItemInDB)
    })

    shoppingListEL.append(newEl)
}

function clearshoppingListEl() {
    shoppingListEL.innerHTML = ""
}
